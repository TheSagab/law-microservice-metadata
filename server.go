package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type Metadata struct {
	gorm.Model
	Filename string `json:"filename"`
	Size     int    `json:"size"`
	MimeType string `json:"mimetype"`
}

func handleRequest(w http.ResponseWriter, r *http.Request, db *gorm.DB) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	log.Println("1")
	ok := auth(r)
	if !ok {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	if r.Method == http.MethodGet {
		log.Println("Received GET Request")
		var metadatas []Metadata
		db.Find(&metadatas)
		js, err := json.Marshal(metadatas)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
		return
	}

	if r.Method == http.MethodPost {
		log.Println("Received POST Request")
		decoder := json.NewDecoder(r.Body)
		var metadata Metadata
		err := decoder.Decode(&metadata)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Print(metadata)
		db.Create(&metadata)
		js, err := json.Marshal(metadata)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
		return
	}

	if r.Method == http.MethodPut {
		log.Println("Received PUT Request")
		decoder := json.NewDecoder(r.Body)
		var metadataFromRequest Metadata
		err := decoder.Decode(&metadataFromRequest)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		var metadataFromDb Metadata
		metadataFromDb.ID = metadataFromRequest.ID
		metadataFromDb.Filename = metadataFromRequest.Filename
		metadataFromDb.Size = metadataFromRequest.Size
		metadataFromDb.MimeType = metadataFromRequest.MimeType
		db.Save(metadataFromDb)
		js, err := json.Marshal(metadataFromDb)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
		return
	}

	if r.Method == http.MethodDelete {
		log.Println("Received DELETE Request")
		decoder := json.NewDecoder(r.Body)
		var metadata Metadata
		err := decoder.Decode(&metadata)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		db.Delete(&metadata)
		js, err := json.Marshal(metadata)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
		return
	}

}

func auth(r *http.Request) bool {
	auth := r.Header.Get("Authorization")
	client := &http.Client{}
	req, err := http.NewRequest("GET", "http://oauth.infralabs.cs.ui.ac.id/oauth/resource", nil)
	if err != nil {
		return false
	}

	req.Header.Set("Authorization", auth)
	resp, err := client.Do(req)
	if err != nil {
		return false
	}

	if resp.StatusCode == 200 {
		return true
	}
	return false
}

func main() {
	db, err := gorm.Open("sqlite3", "metadata.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Metadata{})
	log.Println("Server is listening on port 20104")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		handleRequest(w, r, db)
	})
	http.ListenAndServe(":20104", nil)
}
